##############################################################################
## Example config for el3604
## Oversampling factor can be set by the changing the "NELM" arg to addSlave
## This test script uses the +-320mV sensor configuration       

##############################################################################
epicsEnvSet(EPICS_DRIVER_PATH, "$(PWD)/cellMods:$(EPICS_DRIVER_PATH)")
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="IOC_TEST")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

require ecmccfg develop

# run module startup.cmd (only needed at ESS  PSI auto call at require)
#$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=develop, EC_RATE=1000"
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=6.2.4, EC_RATE=1000"

##############################################################################
## Config hardware:

epicsEnvSet("ECMC_EC_SLAVE_NUM",              "0")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=$(ECMC_EC_SLAVE_NUM), HW_DESC=EK1100"

# Update records in 1ms (all data to EPICS in 1kHz)
epicsEnvSet(ECMC_SAMPLE_RATE_MS,1)

# Note NELM equals oversampling factor in this case. Use +-320mv Config file for all 4 channels
#epicsEnvSet("ECMC_EC_SLAVE_NUM",              "11")
epicsEnvSet("ECMC_EC_SLAVE_NUM",              "1")
${SCRIPTEXEC} ${ecmccfg_DIR}configureSlave.cmd, "SLAVE_ID=$(ECMC_EC_SLAVE_NUM), HW_DESC=ELM3604, NELM=${NELM=20}, CONFIG=-Sensor_+-320mV"

# Update records in 10ms (for all records loaded below)
epicsEnvSet(ECMC_SAMPLE_RATE_MS,10)

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
## Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"

##############################################################################
## Go active:
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)
iocInit()

dbl > pvs.log

